/*

 Lamp Dimmer using SSR-ESP8266 by Research Design Labs.
 http://researchdesignlab.com/wifi-ssr-230v-8a-dimmer.html

 Author: Bandan Das <bsd@makefile.in>
 
 This board relies on 12f1840 for the zero cross detect function
 and firing the triac. To make it talk MQTT, the easiest way is to
 remove the microcontroller and directly connect pin 5 (AC zero cross)
 of MCT2E and pin 5 of MOC3021(firing) to unused GPIO pins on ESP-07.
 
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */
 
#include <ESP8266WiFi.h>
#include <MQTT.h>
#include <EEPROM.h>

//#define DEBUG

#define AP_SSID     "XXX"
#define AP_PASSWORD "XXX"  


#define EIOTCLOUD_USERNAME "YYY"
#define EIOTCLOUD_PASSWORD "YYY"
#define ZERODETECTPIN 12
#define TRIACDRIVEPIN 14
#define PROPAGATIONTIME 8.99

// create MQTT object
#define EIOT_CLOUD_ADDRESS "cloud.iot-playground.com"
MQTT myMqtt("", EIOT_CLOUD_ADDRESS, 1883);

#define CONFIG_START 0
#define CONFIG_VERSION "v01"

struct StoreStruct {
  // This is for mere detection if they are your settings
  char version[4];
  // The variables of your settings
  uint moduleId;  // module id
  uint parameterId; //parameter id
  int  dimval;     // state
} storage = {
  CONFIG_VERSION,
  // The default module 0
  0,
  0 //brightest
};

bool stepOk = false;
String instanceId = "";

boolean result;
String topic("");
String valueStr("");

int dimvalue;
int lastdimval = 0;  /*unused for now*/

void waitOk()
{
  while(!stepOk)
    delay(100);
 
  stepOk = false;
}

void receivedCb(String& topic, String& data) {  
#ifdef DEBUG  
  Serial.print(topic);
  Serial.print(": ");
  Serial.println(data);
#endif
  if (topic == String("/Db/InstanceId"))
  {
    instanceId = data;
    stepOk = true;
  }
  else if (topic ==  String("/Db/"+instanceId+"/NewModule"))
  {
    storage.moduleId = data.toInt();
    stepOk = true;
  }
  else if (topic == String("/Db/"+instanceId+"/"+String(storage.moduleId)+ "/Sensor.Parameter1/NewParameter"))
  {
    stepOk = true;
    storage.parameterId = data.toInt();
  }
  else if (topic == String("/Db/"+instanceId+"/"+String(storage.moduleId)+ "/Settings.Icon1/NewParameter"))
  {
    stepOk = true;
  }
  else if (topic == String("/Db/"+instanceId+"/"+String(storage.moduleId)+ "/Sensor.Parameter1"))
  {
    dimvalue = data.toInt();

#ifdef DEBUG      
    Serial.println("dimming value received");
    Serial.println(dimvalue);
#endif
  }
}

void loadConfig() {
  // To make sure there are settings, and they are YOURS!
  // If nothing is found it will use the default settings.
  if (EEPROM.read(CONFIG_START + 0) == CONFIG_VERSION[0] &&
      EEPROM.read(CONFIG_START + 1) == CONFIG_VERSION[1] &&
      EEPROM.read(CONFIG_START + 2) == CONFIG_VERSION[2])
    for (unsigned int t=0; t<sizeof(storage); t++)
      *((char*)&storage + t) = EEPROM.read(CONFIG_START + t);
}

void saveConfig() {
  for (unsigned int t=0; t<sizeof(storage); t++)
    EEPROM.write(CONFIG_START + t, *((char*)&storage + t));

  EEPROM.commit();
}

void reserConfig()
{
  for (unsigned int t=0; t<sizeof(storage); t++)
    EEPROM.write(CONFIG_START + t, *(char *)0);

    EEPROM.commit();
}

void connectedCb() {
#ifdef DEBUG
  Serial.println("connected to MQTT server");
#endif
  myMqtt.subscribe("/Db/" + instanceId + "/" + String(storage.moduleId) + "/Sensor.Parameter1");
}

void disconnectedCb() {
#ifdef DEBUG
  Serial.println("disconnected. try to reconnect...");
#endif
  delay(500);
  myMqtt.connect();
}

void publishedCb() {
#ifdef DEBUG  
  Serial.println("published.");
#endif
}

String macToStr(const uint8_t* mac)
{
  String result;
  for (int i = 0; i < 6; ++i) {
    result += String(mac[i], 16);
    if (i < 5)
      result += ':';
  }
  return result;
}

void dimmer()
{
  int delaytime = 75.89 * storage.dimval;
  if (delaytime == 0) {
    //switch off
    digitalWrite(TRIACDRIVEPIN, LOW);
    return;
  }
  
  delayMicroseconds(delaytime);
  digitalWrite(TRIACDRIVEPIN, HIGH);
  delayMicroseconds(PROPAGATIONTIME);
  digitalWrite(TRIACDRIVEPIN, LOW);
}

void setup() 
{  
//Serial.begin(115200);
  delay(1000);

  pinMode(ZERODETECTPIN, INPUT);
  pinMode(TRIACDRIVEPIN, OUTPUT);
  digitalWrite(ZERODETECTPIN, LOW);
  digitalWrite(TRIACDRIVEPIN, LOW);
  //pinMode(BUILTIN_LED, OUTPUT);

#ifdef DEBUG
  Serial.println("Done setting up pin configuration...");
#endif

  EEPROM.begin(512);

#ifdef DEBUG
  Serial.println("Loading config from EEPROM...");
#endif
  loadConfig();

#ifdef DEBUG
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(AP_SSID);
#endif  
  WiFi.begin(AP_SSID, AP_PASSWORD);
  
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(BUILTIN_LED, HIGH);
    delay(1000);
    digitalWrite(BUILTIN_LED, LOW);
#ifdef DEBUG
    Serial.print("Not Connected");
#endif
  }
  digitalWrite(BUILTIN_LED, HIGH);
  
#ifdef DEBUG
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Connecting to MQTT server");  
#endif
  //set client id
  // Generate client name based on MAC address and last 8 bits of microsecond counter
  String clientName;
  //clientName += "esp8266-";
  uint8_t mac[6];
  WiFi.macAddress(mac);
  clientName += macToStr(mac);
  clientName += "-";
  clientName += String(micros() & 0xff, 16);
  myMqtt.setClientId((char*) clientName.c_str());

#ifdef DEBUG
  Serial.print("MQTT client id:");
  Serial.println(clientName);
#endif
  // setup callbacks
  myMqtt.onConnected(connectedCb);
  myMqtt.onDisconnected(disconnectedCb);
  myMqtt.onPublished(publishedCb);
  myMqtt.onData(receivedCb);

#ifdef DEBUG
  Serial.println("Setup callbacks...");
#endif

  //////Serial.println("connect mqtt...");
  myMqtt.setUserPwd(EIOTCLOUD_USERNAME, EIOTCLOUD_PASSWORD);  
  myMqtt.connect();

  delay(500);

#ifdef DEBUG
  Serial.println("Connected to MQTT broker...");
#endif

  //get instance id
  //////Serial.println("suscribe: Db/InstanceId");
  myMqtt.subscribe("/Db/InstanceId");

  waitOk();

#ifdef DEBUG
  Serial.print("InstanceID:");
  Serial.println(instanceId);
#endif

  //create module if necessary 
  if (storage.moduleId == 0)
  {
    //create module
#ifdef DEBUG
    Serial.println("create module: Db/"+instanceId+"/NewModule");
#endif
    myMqtt.subscribe("/Db/"+instanceId+"/NewModule");
    waitOk();
      
    // create Sensor.Parameter1
#ifdef DEBUG    
    Serial.println("/Db/"+instanceId+"/"+String(storage.moduleId)+ "/Sensor.Parameter1/NewParameter");    
#endif
    myMqtt.subscribe("/Db/"+instanceId+"/"+String(storage.moduleId)+ "/Sensor.Parameter1/NewParameter");
    waitOk();

    // set module type
#ifdef DEBUG        
    Serial.println("Set module type to analog output");
#endif
    valueStr = "MT_ANALOG_OUTPUT";
    topic  = "/Db/" + instanceId + "/" + String(storage.moduleId) + "/ModuleType";
    result = myMqtt.publish(topic, valueStr);
    delay(100);

#ifdef DEBUG        
    Serial.println("Set parameter unit to %");
#endif

    valueStr = "%";
    topic  = "/Db/" + instanceId + "/" + String(storage.moduleId) + "/Sensor.Parameter1/Unit";
    result = myMqtt.publish(topic, valueStr);
    delay(100);
    
    // save new module id
    saveConfig();
  }

  
  //publish dimmer state
  valueStr = String(storage.dimval);
  topic  = "/Db/"+instanceId+"/"+String(storage.moduleId)+ "/Sensor.Parameter1";
  result = myMqtt.publish(topic, valueStr);

  dimvalue = storage.dimval;

#ifdef DEBUG        
    Serial.print("Stored dimmer value is:");
    Serial.println(dimvalue);
#endif

  myMqtt.subscribe("/Db/"+instanceId+"/"+String(storage.moduleId)+ "/Sensor.Parameter1");
  attachInterrupt(ZERODETECTPIN, dimmer, RISING);
#ifdef DEBUG        
    Serial.println("Setup complete!");
#endif
}



void loop() {
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(BUILTIN_LED, LOW);
    delay(500);
    digitalWrite(BUILTIN_LED, HIGH);
#ifdef DEBUG        
    Serial.print("Wifi Not Connected...");
#endif
  }  

  if (dimvalue != storage.dimval)
  {
    storage.dimval = dimvalue;
    // save dimmer value
#ifdef DEBUG        
    Serial.println("Detected upstream change in dimmer value");
    Serial.print("New Value:");
    Serial.println(storage.dimval);
#endif
    saveConfig();
#ifdef DEBUG
    Serial.println("Saving new config to EEPROM...");
#endif
  }  
}


